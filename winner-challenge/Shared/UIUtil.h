//
//  Util.h
//  winner-challenge
//
//  Created by Igor Silva on 5/8/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIUtil : NSObject

typedef void(^CrossCallback)(void(^success)(NSString*), void(^failure)(NSString*), void(^end)());

+ (void)shadifyView:(UIView*)view;

+ (void)roundifyView:(UIView*)view;

+ (void)showHUDinView:(UIView*)view
            withLabel:(NSString*)text
           andExecute:(CrossCallback)block;

+ (void)showPromptWithTitle:(NSString*)title
                 andMessage:(NSString*)message
         intoViewController:(UIViewController*)controller
                 yesHandler:(void(^)(UIAlertAction*))yesHandler
                  noHandler:(void(^)(UIAlertAction*))noHandler;

+ (void)showAlertWithTitle:(NSString*)title
                andMessage:(NSString*)message
        intoViewController:(UIViewController*)controller
            withCompletion:(void(^)(UIAlertAction*))completion;

@end
