//
//  Util.m
//  winner-challenge
//
//  Created by Igor Silva on 5/8/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "UIUtil.h"
#import "MBProgressHUD.h"

#define SUCCESS_FAILURE_HIDE_DELAY 2 //seconds

@implementation UIUtil

// Setup a shadow for the UIView
+ (void)shadifyView:(UIView*)view
{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset  = CGSizeMake(0, 1);
    view.layer.shadowRadius  = 1;
    view.layer.shadowOpacity = 0.3;
}

// Make the UIView's corner round
+ (void)roundifyView:(UIView*)view
{
    view.layer.cornerRadius = 2;
}

// Show a progress HUD with success and failure completion
+ (void)showHUDinView:(UIView*)view
            withLabel:(NSString*)text
           andExecute:(CrossCallback)block {

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = text;

    // Fire off an asynchronous task, giving UIKit the opportunity to redraw
    // with the HUD added to the view hierarchy
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{

        void(^show)(NSString*, NSString*) = ^void(NSString* imgName, NSString* txt)
        {
            hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgName]];
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = txt;

            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hide:YES afterDelay:SUCCESS_FAILURE_HIDE_DELAY];
            });
        };

        block(^void(NSString *msg) {
            show(@"checkmark", msg);  // SUCCESS
        }, ^void(NSString *msg) {
            show(@"error", msg);      // FAILURE
        }, ^void() {
            dispatch_async(dispatch_get_main_queue(), ^{
              [hud hide:YES];         // END
            });
        });
    });
}

+ (void)showAlertWithTitle:(NSString*)title
                andMessage:(NSString*)message
        intoViewController:(UIViewController*)controller
            withCompletion:(void(^)(UIAlertAction*))completion {

    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:completion];

    [alert addAction:okButton];

    [controller presentViewController:alert animated:YES completion:nil];
}

+ (void)showPromptWithTitle:(NSString*)title
                 andMessage:(NSString*)message
         intoViewController:(UIViewController*)controller
                 yesHandler:(void(^)(UIAlertAction*))yesHandler
                  noHandler:(void(^)(UIAlertAction*))noHandler {

    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"No"
                                                       style:UIAlertActionStyleDefault
                                                     handler:noHandler];

    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:yesHandler];
    
    [alert addAction:noButton];
    [alert addAction:yesButton];
    
    [controller presentViewController:alert animated:YES completion:nil];
}

@end
