//
//  Ride.m
//  winner-challenge
//
//  Created by Igor Silva on 5/14/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "Ride.h"
#import "SWNetworking.h"

@interface Ride()

// Redefine properties as readwrite so it's only assignable from inside this class
@property (nonatomic, assign, readwrite) NSInteger rideId;
@property (nonatomic, strong, readwrite) NSString *msg;
@property (nonatomic, strong, readwrite) NSDate *lastUpdate;
@property (nonatomic, strong, readwrite) NSDictionary *options;
@property (nonatomic, assign, readwrite) CLLocationCoordinate2D destination;

@end

@implementation Ride {
    NSString *_location;
}

const static NSString *source   = @"http://ec2-54-88-12-34.compute-1.amazonaws.com";
const static NSString *endpoint = @"/v1/ride";

- (instancetype)initWithLocation:(NSString*)location
                     destination:(CLLocationCoordinate2D)dest
                         options:(NSDictionary*)options {

    self.options = options;
    self.destination = dest;

    _location = location;

    [self sync];

    return self;
}

- (void)sync
{
    // Create the request
    SWGETRequest *request = [[SWGETRequest alloc] init];
    request.responseDataType = [SWResponseJSONDataType type];

    // Perform the request - GET _location
    [request startDataTaskWithURL:_location parameters:nil

        success: ^(NSURLSessionDataTask *task, NSDictionary *response) {

            NSHTTPURLResponse* httpResponse = ((NSHTTPURLResponse*)task.response);

            if (httpResponse.statusCode == 200)
            {
                self.lastUpdate = [NSDate date]; // now
                self.msg        =       response[@"msg"];
                self.rideId     = (long)response[@"rideId"];
            }

        } failure: nil
    ];
}

+ (void)createRideWithOptions:(NSDictionary*)options
                  destination:(CLLocationCoordinate2D)dest
                   completion:(RideSuccessCallback)success
                      failure:(RideFailureCallback)failure {

    // Build URL
    NSString *path = [NSString stringWithFormat:@"%@%@", source, endpoint];

    // Create the request
    SWPUTRequest *request = [[SWPUTRequest alloc] init];
    request.responseDataType = [SWResponseJSONDataType type];

    // Perform the request - PUT /v1/ride
    [request startDataTaskWithURL:path parameters:nil

        // Just got a response, still have to check status code and data
        success:^(NSURLSessionDataTask *task, NSDictionary *response) {

            NSHTTPURLResponse* httpResponse = ((NSHTTPURLResponse*)task.response);

            switch (httpResponse.statusCode)
            {
                // Ride successfully createad
                case 201: {
                    NSString *location = httpResponse.allHeaderFields[@"Location"];
                    Ride *ride = [[Ride alloc] initWithLocation:location destination:dest options:options];
                    success(ride);
                } break;

                // Any error happend on the server-side
                case 410: return failure(response[@"msg"]); break;

                // Unknown
                default: return failure(UNKNOWN_ERROR); break;
            }
        }

        // Couldn't perform the request
        failure: ^(NSURLSessionTask *task, NSError *error) {
            return failure(UNKNOWN_ERROR);
        }
    ];
}

@end
