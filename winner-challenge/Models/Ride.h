//
//  Ride.h
//  winner-challenge
//
//  Created by Igor Silva on 5/14/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

#define UNKNOWN_ERROR @"UNKWON"

@interface Ride : NSObject

typedef void(^RideSuccessCallback)(Ride *ride);
typedef void(^RideFailureCallback)(NSString *errorMsg);

@property (nonatomic, assign, readonly) NSInteger rideId;
@property (nonatomic, strong, readonly) NSString *msg;
@property (nonatomic, strong, readonly) NSDate *lastUpdate;
@property (nonatomic, strong, readonly) NSDictionary *options;
@property (nonatomic, assign, readonly) CLLocationCoordinate2D destination;

+ (void)createRideWithOptions:(NSDictionary*)options
                  destination:(CLLocationCoordinate2D)dest
                   completion:(RideSuccessCallback)success
                      failure:(RideFailureCallback)failure;
- (void)sync;

@end
