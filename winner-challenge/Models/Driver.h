//
//  Driver.h
//  winner-challenge
//
//  Created by Igor Silva on 5/15/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

@interface Driver : NSObject

@property (nonatomic, assign, readonly) NSInteger driverId;
@property (nonatomic, assign, readonly) BOOL available;
@property (nonatomic, assign, readwrite) CLLocationCoordinate2D location;

+ (void)getAvailableDriversWithSW:(CLLocationCoordinate2D)sw
                               NE:(CLLocationCoordinate2D)ne
                       completion:(void(^)(NSArray<Driver*> *drivers))completion;

@end
