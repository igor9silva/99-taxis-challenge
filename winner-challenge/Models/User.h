//
//  User.h
//  winner-challenge
//
//  Created by Igor Silva on 5/15/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UNKNOWN_ERROR @"UNKWON"

@interface User : NSObject

typedef void(^UserSuccessCallback)();
typedef void(^UserFailureCallback)(NSString *errorMsg);

@property (nonatomic, assign, readonly) NSInteger _id;
@property (nonatomic, strong, readonly) NSString  *name;

// Grab the logged in user or nil
+ (User*)loggedInUser;

// Log the user out
+ (void)logOut;

// Signup a new user
+ (void)signUpWithName:(NSString*)name
           andPassword:(NSString*)password
        withCompletion:(UserSuccessCallback)success
               failure:(UserFailureCallback)failure;

@end
