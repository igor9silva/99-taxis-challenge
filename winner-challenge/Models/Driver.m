//
//  Driver.m
//  winner-challenge
//
//  Created by Igor Silva on 5/15/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "Driver.h"
#import "SWNetworking.h"

@implementation Driver

const static NSString *source   = @"https://api.99taxis.com";
const static NSString *endpoint = @"/lastLocations";

- (instancetype)initWithID:(NSInteger)driverId
                  location:(CLLocationCoordinate2D)coord
              availability:(BOOL)available {

    _driverId  = driverId;
    _location  = coord;
    _available = available;

    return self;
}

+ (void)getAvailableDriversWithSW:(CLLocationCoordinate2D)sw
                               NE:(CLLocationCoordinate2D)ne
                       completion:(void(^)(NSArray<Driver*> *drivers))completion {

    // Create the request
    SWGETRequest *request = [[SWGETRequest alloc] init];
    request.responseDataType = [SWResponseJSONDataType type];

    // GET parameters
    NSString *SW = [NSString stringWithFormat:@"%f,%f", sw.latitude, sw.longitude];
    NSString *NE = [NSString stringWithFormat:@"%f,%f", ne.latitude, ne.longitude];
    const NSDictionary *params = @{ @"sw": SW, @"ne": NE };

    // Build URL
    NSString *path = [NSString stringWithFormat:@"%@%@", source, endpoint];

    // Perform the request - GET _location
    [request startDataTaskWithURL:path parameters:params

        success: ^(NSURLSessionDataTask *task, NSArray *response) {

            NSHTTPURLResponse* httpResponse = ((NSHTTPURLResponse*)task.response);

            if (httpResponse.statusCode == 200)
            {
                NSMutableArray *arr = [[NSMutableArray alloc] init];

                for (NSDictionary *item in response)
                {
                    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([item[@"latitude"] doubleValue],
                                                                                 [item[@"longitude"] doubleValue]);

                    [arr addObject:[[Driver alloc] initWithID:[item[@"driverId"] integerValue]
                                                     location:location
                                                 availability:item[@"driverAvailable"]]];
                }

                completion(arr);
            }
            else
            {
                completion(@[]);
            }

        } failure: nil
    ];
}

@end
