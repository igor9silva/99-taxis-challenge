//
//  User.m
//  winner-challenge
//
//  Created by Igor Silva on 5/15/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "User.h"
#import "SWNetworking.h"

@interface User()

// Redefine properties as readwrite so it's only assignable from inside this class
@property (nonatomic, assign, readwrite) NSInteger _id;
@property (nonatomic, strong, readwrite) NSString  *name;

@end

@implementation User

const static NSString *source   = @"http://ec2-54-88-12-34.compute-1.amazonaws.com";
const static NSString *endpoint = @"/v1/users";

- (instancetype)initWithID:(NSInteger)_id andName:(NSString*)name
{
    self._id = _id;
    self.name = name;

    return self;
}

+ (User*)loggedInUser
{
    const NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSInteger _id  = [defaults integerForKey:@"user.id"];
    NSString *name = [defaults stringForKey:@"user.name"];

    // Got value from NSUserDefaults

    if (_id != 0 && name != nil) {
        return [[User alloc] initWithID:_id andName:name];
    } else {
        return nil;
    }
}

+ (void)logOut
{
    const NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [defaults removeObjectForKey:@"user.name"];
    [defaults removeObjectForKey:@"user.id"];

    [defaults synchronize];
}

+ (void)signUpWithName:(NSString*)name
           andPassword:(NSString*)password
        withCompletion:(UserSuccessCallback)success
               failure:(UserFailureCallback)failure {

    // Create the request
    SWPOSTRequest *request = [[SWPOSTRequest alloc] init];
    request.responseDataType = [SWResponseJSONDataType type];

    // POST x-www-form-urlencoded parameters
    const NSDictionary *params = @{ @"name" : name };

    // Build URL
    NSString *path = [NSString stringWithFormat:@"%@%@", source, endpoint];

    // Perform the request - POST /v1/users {name: 'name'}
    [request startDataTaskWithURL:path parameters:params

          success:^(NSURLSessionDataTask *task, NSDictionary *response) {

              NSHTTPURLResponse* httpResponse = ((NSHTTPURLResponse*)task.response);

              switch (httpResponse.statusCode)
              {
                  // User successfully createad
                  case 201: {
                      [User userLoggedIn:httpResponse.allHeaderFields[@"Location"]];
                      return success();
                  } break;

                  // Invalid name
                  case 410: return failure(response[@"message"]); break;
                      
                  // Unknown
                  default: return failure(UNKNOWN_ERROR); break;
              }

          }

          failure:^(NSURLSessionTask *uploadTask, NSError *error) {
              return failure(UNKNOWN_ERROR);
          }
    ];
}

+ (void)userLoggedIn:(NSString*)location
{
    const NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    // Create the request
    SWGETRequest *request = [[SWGETRequest alloc] init];
    request.responseDataType = [SWResponseJSONDataType type];

    // Perform the request - GET _location
    [request startDataTaskWithURL:location parameters:nil

          success: ^(NSURLSessionDataTask *task, NSDictionary *response) {

              NSHTTPURLResponse* httpResponse = ((NSHTTPURLResponse*)task.response);

              if (httpResponse.statusCode == 200)
              {
                  // Persist user data into NSUserDefaults
                  // Do not keep in memory since NSUserDefaults already take care of it

                  [defaults setInteger:(NSInteger)response[@"id"]   forKey:@"user.id"];
                  [defaults setObject:            response[@"name"] forKey:@"user.name"];
                  
                  [defaults synchronize];
              }
              
          } failure: nil
     ];
}

@end
