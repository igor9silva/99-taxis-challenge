//
//  CLPlacemark+Helpers.m
//  winner-challenge
//
//  Created by Igor Silva on 5/15/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "CLPlacemark+Helpers.h"

@implementation CLPlacemark (Helpers)

- (NSString *)description
{
    // TODO: check one by one if it's populated

    return [NSString stringWithFormat:@"%@, %@ - %@, %@ - %@, %@",
             self.thoroughfare,
             self.subThoroughfare,
             self.subLocality,
             self.locality,
             //self.subAdministrativeArea,
             self.administrativeArea,
             self.country];
}

@end
