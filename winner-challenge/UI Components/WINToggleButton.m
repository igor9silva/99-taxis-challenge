//
//  WINToggleButton.m
//  winner-challenge
//
//  Created by Igor Silva on 5/8/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "WINToggleButton.h"
#import "IonIcons.h"

#define kIconWidth 30 // pixels
#define kIconSize 15  // pixels
#define kIconColor [UIColor colorWithRed:0.199 green:0.597 blue:1 alpha:1]

@interface WINToggleButton()

// Redefine property as readwrite so it's only assignable from inside this class
@property (nonatomic, readwrite, assign, getter = isPressed) BOOL pressed;

@end

@implementation WINToggleButton {
    UIImageView *_rightView;
    UIImage *_selectedIcon;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];

    if (self) [self setup];

    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self) [self setup];

    return self;
}

- (void)setup
{
    // Setup right icon
    _selectedIcon = [IonIcons imageWithIcon:ion_checkmark_round
                                  iconColor:kIconColor
                                   iconSize:kIconSize
                                  imageSize:CGSizeMake(kIconWidth, self.frame.size.height)];

    // Setup right view (where the icon goes)
    _rightView = [[UIImageView alloc] init];
    [self addSubview:_rightView];

    // Add a listener for TouchUpInside
    [self addTarget:self action:@selector(didTouchButton) forControlEvents:UIControlEventTouchUpInside];

    self.pressed = NO;
}

- (void)drawRect:(CGRect)rect
{
    // Update right view frame
    _rightView.frame = CGRectMake(rect.size.width - kIconWidth, // X
                                  0,                            // Y
                                  kIconWidth,                   // Width
                                  rect.size.height);            // Heigth
}

- (void)setPressed:(BOOL)pressed
{
    // Update icon
    _rightView.image = pressed? _selectedIcon : nil;

    // Set the iVar
    _pressed = pressed;
}

- (void)didTouchButton
{
    self.pressed = !self.pressed;
}

@end
