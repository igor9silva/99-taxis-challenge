//
//  WINTaxiMarker.m
//  winner-challenge
//
//  Created by Igor Silva on 5/15/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "WINTaxiMarker.h"

#define kIcon [UIImage imageNamed:@"taxi"]

@implementation WINTaxiMarker

+ (instancetype)markerWithPosition:(CLLocationCoordinate2D)position
{
    return [super markerWithPosition:position icon:kIcon];
}

@end
