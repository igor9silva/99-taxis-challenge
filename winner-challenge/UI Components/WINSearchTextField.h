//
//  WINSearchTextField.h
//  winner-challenge
//
//  Created by Igor Silva on 5/8/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WINSearchTextField : UITextField

@end
