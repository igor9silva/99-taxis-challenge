//
//  WINMarker.m
//  winner-challenge
//
//  Created by Igor Silva on 5/15/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "WINMarker.h"

@implementation WINMarker

+ (instancetype)markerWithPosition:(CLLocationCoordinate2D)position
{
    WINMarker *marker = [super markerWithPosition:position];
    marker.appearAnimation = kGMSMarkerAnimationPop;

    return marker;
}

+ (instancetype)markerWithPosition:(CLLocationCoordinate2D)position icon:(UIImage*)icon
{
    WINMarker *marker = [WINMarker markerWithPosition:position];
    marker.icon = icon;

    return marker;
}

@end
