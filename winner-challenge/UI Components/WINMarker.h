//
//  WINMarker.h
//  winner-challenge
//
//  Created by Igor Silva on 5/15/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@interface WINMarker : GMSMarker

+ (instancetype)markerWithPosition:(CLLocationCoordinate2D)position icon:(UIImage*)icon;

@end
