//
//  WINButton.m
//  winner-challenge
//
//  Created by Igor Silva on 5/8/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "WINButton.h"
#import "UIUtil.h"

@implementation WINButton

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];

    if (self) [self setup];

    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self) [self setup];

    return self;
}

- (void)setup
{
    // Add a shadow
    [UIUtil shadifyView:self];

    // Make it's corners round
    [UIUtil roundifyView:self];
}

@end
