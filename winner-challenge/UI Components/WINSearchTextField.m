//
//  WINSearchTextField.m
//  winner-challenge
//
//  Created by Igor Silva on 5/8/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "WINSearchTextField.h"
#import "IonIcons.h"
#import "UIUtil.h"

#define kSearchIconSize 20
#define kCloseIconSize 15
#define kRightViewWidth 40

@implementation WINSearchTextField {
    UIImage *closeIcon;
    UIImage *searchIcon;
}

- (instancetype)initWithCoder:(NSCoder*)aDecoder
{
    self = [super initWithCoder:aDecoder];

    if (self) [self setup];

    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self) [self setup];

    return self;
}

- (void)dealloc
{
    [self removeTarget:self action:@selector(editingChanged:) forControlEvents:UIControlEventEditingChanged];
}

- (void)setup
{
    // Add a shadow
    [UIUtil shadifyView:self];

    // Make it's corners round
    [UIUtil roundifyView:self];

    closeIcon = [IonIcons imageWithIcon:ion_close_circled size:kCloseIconSize color:[UIColor grayColor]];
    searchIcon = [IonIcons imageWithIcon:ion_ios_search_strong size:kSearchIconSize color:[UIColor grayColor]];

    // Add a left and right view for padding the text
    UIImageView *rightView = [[UIImageView alloc] initWithImage:searchIcon];

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self
                                             action:@selector(tapRightView:)];

    [rightView addGestureRecognizer:tapRecognizer];
    rightView.userInteractionEnabled = YES;
    rightView.contentMode = UIViewContentModeCenter;
    rightView.frame = CGRectMake(0, 0, kRightViewWidth, self.frame.size.height);

    self.leftView      = rightView;
    self.rightView     = rightView;
    self.leftViewMode  = UITextFieldViewModeAlways;
    self.rightViewMode = UITextFieldViewModeAlways;

    [self addTarget:self action:@selector(editingChanged:) forControlEvents:UIControlEventEditingChanged];
}

- (void)tapRightView:(id)sender
{
    self.text = @"";
    [self becomeFirstResponder];
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    [self updateIcon];
}

- (void)editingChanged:(UITextField*)textField
{
    [self updateIcon];
}

- (void)updateIcon
{
    UIImageView *view = (UIImageView*)self.rightView;
    view.image = (self.text.length == 0)? searchIcon : closeIcon;
}

@end
