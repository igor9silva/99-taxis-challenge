//
//  SignUpViewController.h
//  winner-challenge
//
//  Created by Igor Silva on 5/15/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XLForm/XLForm.h>

#import "User.h"

@interface SignUpViewController : XLFormViewController

@property (strong, nonatomic) IBOutlet UINavigationBar *navBar;
@property (copy, nonatomic) void (^callback)();

- (IBAction)didTouchSubmit:(id)sender;
- (IBAction)didTouchCancel:(id)sender;

@end
