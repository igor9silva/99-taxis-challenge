//
//  MainViewController.h
//  winner-challenge
//
//  Created by Igor Silva on 5/7/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

@import GoogleMaps;

#import <UIKit/UIKit.h>
#import "WINButton.h"
#import "WINToggleButton.h"

// UI
#define kInitialAnimationDuration 0.4
#define kInitialScaleFactor 7

// CoreLocation
#define kDistanceFilter 10 // meters

@interface MainViewController : UIViewController<GMSMapViewDelegate, CLLocationManagerDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet GMSMapView *viewMap;
@property (strong, nonatomic) IBOutlet UIView *viewBackground;
@property (strong, nonatomic) IBOutlet UITextField *txtSearch;
@property (strong, nonatomic) IBOutlet UIButton *btnCallTaxi;
@property (strong, nonatomic) IBOutletCollection(WINToggleButton) NSArray *options;

@property (assign, nonatomic) CLLocationCoordinate2D destination;

- (IBAction)btnCallTaxi_touchUpInside:(WINButton*)sender;

@end
