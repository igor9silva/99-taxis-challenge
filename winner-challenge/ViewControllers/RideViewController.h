//
//  RideViewController.h
//  winner-challenge
//
//  Created by Igor Silva on 5/14/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ride.h"

@interface RideViewController : UIViewController

@property (nonatomic, strong, readwrite) IBOutlet UILabel *lblMsg;
@property (nonatomic, strong, readwrite) Ride *ride;

- (IBAction)dismiss:(id)sender;
- (IBAction)arrived:(id)sender;

@end
