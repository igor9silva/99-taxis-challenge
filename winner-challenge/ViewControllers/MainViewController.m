//
//  MainViewController.m
//  winner-challenge
//
//  Created by Igor Silva on 5/7/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "MainViewController.h"
#import "RideViewController.h"
#import "SignUpViewController.h"

#import "Ride.h"
#import "Driver.h"
#import "UIUtil.h"
#import "WINMarker.h"
#import "WINTaxiMarker.h"
#import "CLPlacemark+Helpers.h"

#define kLoadDriversInterval 3 // seconds
#define kMinZoomToLoadDrivers 13

@implementation MainViewController {
    CLGeocoder *_geocoder;
    NSTimer *_loadDriversTimer;
    WINMarker *_destinationMarker;
    GMSVisibleRegion _visibleRegion;
    CLLocationManager *_locationManager;
    NSMutableDictionary<NSNumber*, WINTaxiMarker*> *_driverMarkers;
}

#pragma mark - LIFECYCLE

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.txtSearch.alpha = 0;
    self.txtSearch.delegate = self;

    self.viewMap.alpha = 0;
    self.viewMap.delegate = self;

    [self initLocationManager];
    _geocoder = [[CLGeocoder alloc] init];

    _driverMarkers = [[NSMutableDictionary alloc] init];

    // Schedule a timer to check for available drivers each 'kLoadDriversInterval' seconds.
    _loadDriversTimer = [NSTimer scheduledTimerWithTimeInterval:kLoadDriversInterval
                                                         target:self
                                                       selector:@selector(loadDrivers:)
                                                       userInfo:nil
                                                        repeats:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self fadeOutLaunchScreenWithCompletion:nil];
    [self fadeInViewUIWithCompletion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

    // Clear all driver marks
    for (WINTaxiMarker *marker in _driverMarkers) {
        marker.map = nil;
    }

    _driverMarkers = [[NSMutableDictionary alloc] init];
}

- (void)dealloc
{
    [_loadDriversTimer invalidate];
}

#pragma mark - INITALIZATION

- (void)initLocationManager
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kDistanceFilter;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;

    // Request user authorization, will startUpdating when granted
    [_locationManager requestWhenInUseAuthorization];
}

#pragma mark - DELEGATES

#pragma mark UITextFieldDelegate

// When return/enter was pressed
- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    // Resign the focus
    [textField resignFirstResponder];

    if (textField.text.length == 0) return NO;

    [UIUtil showHUDinView:self.viewMap withLabel:@"Searching..."
        andExecute:^(void (^success)(NSString *), void (^failure)(NSString *), void (^end)(void)) {

            // Geocode typed address into a coordinate
            [_geocoder geocodeAddressString:self.txtSearch.text
                 completionHandler:^(NSArray<CLPlacemark*> *placemarks, NSError *error)
                 {
                     if (placemarks.count > 0) {
                         self.destination = placemarks[0].location.coordinate;
                         end();
                     } else {
                         failure(@"Couldn't find that address :(");
                     }
                 }
             ];
        }
    ];

    return NO;
}

- (void)textFieldDidEndEditing:(UITextField*)textField
{
    if (textField.text.length == 0
        && self.destination.latitude != 0
        && self.destination.longitude != 0) {

        // Set self.destination to itself to trigger textField setter
        [self setDestination:self.destination];
    }
}

#pragma mark GMSMapViewDelegate

// When user stops dragging/zooming
- (void)mapView:(GMSMapView*)mapView idleAtCameraPosition:(GMSCameraPosition*)position
{
    _visibleRegion = self.viewMap.projection.visibleRegion;
}

// When user long-pressed on the map
- (void)mapView:(GMSMapView*)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coord
{
    self.destination = coord;
}

#pragma mark CLLocationManagerDelegate

// When received a new location
- (void)locationManager:(CLLocationManager*)manager
     didUpdateLocations:(NSArray<CLLocation*>*)locations {

    // If destination isn't set
    if (self.destination.latitude == 0 && self.destination.longitude == 0) {
        self.destination = locations.lastObject.coordinate;
    }
}

// When authorization status changed (authorized or not)
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status)
    {
        // Denied/Restricted
        case kCLAuthorizationStatusDenied:
        case kCLAuthorizationStatusRestricted: {
            NSLog(@"User hasn't authorized :(");
        } break;

        // Authorized
        case kCLAuthorizationStatusAuthorizedAlways:
        case kCLAuthorizationStatusAuthorizedWhenInUse: {
            [_locationManager startUpdatingLocation];
        } break;

        default: break;
    }
}


#pragma mark - UI

#pragma mark Animations

- (void)fadeOutLaunchScreenWithCompletion:(void(^)(BOOL))completion
{
    const CGAffineTransform scaleIn = CGAffineTransformScale(CGAffineTransformIdentity,
                                                             kInitialScaleFactor,
                                                             kInitialScaleFactor);

    [UIView animateWithDuration:kInitialAnimationDuration animations:^{
        self.viewBackground.alpha = 0;
        self.viewBackground.transform = scaleIn;
    } completion: completion];
}

- (void)fadeInViewUIWithCompletion:(void(^)(BOOL))completion
{
    [UIView animateWithDuration:kInitialAnimationDuration animations:^{
        self.viewMap.alpha = 1;
        self.txtSearch.alpha = 1;
    } completion:completion];
}

#pragma mark - EVENTS

- (IBAction)btnCallTaxi_touchUpInside:(WINButton *)sender
{
    [self optionsDict];
    if (_destination.latitude == 0 && _destination.longitude == 0) {
        [self.txtSearch becomeFirstResponder];
    } else if ([User loggedInUser] != nil) {
        [self searchRide];
    } else {
        [self performSegueWithIdentifier:@"signUpSegue" sender:^{
            [self searchRide];
        }];
    }
}

- (void)searchRide
{
    [UIUtil showHUDinView:self.view withLabel:@"Looking for drivers..."
        andExecute:^(void (^success)(NSString*), void (^failure)(NSString*), void (^end)(void))
        {
            [Ride createRideWithOptions:[self optionsDict] destination:self.destination
            completion:^(Ride *ride)
            {
                // Show ride status modal
                [self performSegueWithIdentifier:@"rideSegue" sender:ride];

                end(); // Dismiss HUD

            } failure:failure];
        }
    ];
}

-(void)loadDrivers:(NSTimer*)timer
{
    if (self.viewMap.camera.zoom > kMinZoomToLoadDrivers)
    {
        CLLocationCoordinate2D sw = _visibleRegion.nearLeft;
        CLLocationCoordinate2D ne = _visibleRegion.farRight;

        [Driver getAvailableDriversWithSW:sw NE:ne completion:^(NSArray<Driver*> *drivers) {
            for (Driver *driver in drivers) {
                [self addOrUpdateDriverMarker:driver.driverId position:driver.location];
            }
        }];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"rideSegue"])
    {
        RideViewController *vc = segue.destinationViewController;
        vc.ride = sender;
    }
    else if ([segue.identifier isEqualToString:@"signUpSegue"])
    {
        SignUpViewController *vc = segue.destinationViewController;
        vc.callback = sender;
    }
}

#pragma mark - ACCESSORS

- (void)setDestination:(CLLocationCoordinate2D)coord
{
    // Add or update the marker's position
    if (_destinationMarker == nil)
    {
        _destinationMarker = [WINMarker markerWithPosition:coord];
        _destinationMarker.map = self.viewMap;
    }
    else
    {
        _destinationMarker.position = coord;
    }

    // Get a CLLocation from the given coordinates
    CLLocation *location = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];

    // Reverse geocode the location, then update the search text box
    [_geocoder reverseGeocodeLocation:location
        completionHandler:^(NSArray<CLPlacemark*> *placemarks, NSError *error)
        {
            if (placemarks.count > 0) {
                self.txtSearch.text = placemarks[0].description;
            } else {
                self.txtSearch.text = [NSString stringWithFormat:@"%f - %f", coord.latitude, coord.longitude];
            }
        }
    ];

    // Set the iVar
    _destination = coord;

    // Move the map focus to new destination
    [self.viewMap animateToCameraPosition:[GMSCameraPosition cameraWithTarget:coord zoom:16]];
}

- (void)addOrUpdateDriverMarker:(NSInteger)_id position:(CLLocationCoordinate2D)coord
{
    WINTaxiMarker *marker = _driverMarkers[@(_id)];

    if (marker == nil) {
        marker = [WINTaxiMarker markerWithPosition:coord];
        marker.map = self.viewMap;
        [_driverMarkers setObject:marker forKey:@(_id)];
    } else {
        marker.position = coord;
    }
}

- (NSDictionary*)optionsDict
{
    const NSDictionary* keys = @{
        @0: @"large-trunk",
        @1: @"pet-friendly",
        @2: @"air-conditioner",
        @3: @"payment-cash",
        @4: @"payment-debit",
        @5: @"payment-credit",
    };

    NSMutableDictionary *ret = [[NSMutableDictionary alloc] initWithCapacity:6];

    for (WINToggleButton *opt in self.options) {
        [ret setValue:@(opt.pressed) forKey:keys[@(opt.tag)]];
    }

    return ret;
}
@end
