//
//  SignUpViewController.m
//  winner-challenge
//
//  Created by Igor Silva on 5/15/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "SignUpViewController.h"

#import "UIUtil.h"

#define kPasswordMinLength 4
#define kNameMinLength 3

@implementation SignUpViewController

#pragma mark - LIFECYCLE

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initForm];
    [self initContraints];
}

#pragma mark - INITALIZATION

- (void)initForm
{
    XLFormDescriptor *form;
    XLFormSectionDescriptor *section;
    XLFormRowDescriptor *row;

    form = [XLFormDescriptor formDescriptor];
    form.assignFirstResponderOnShow = YES;

    // Name Section
    section = [XLFormSectionDescriptor formSectionWithTitle:@"Name"];
    [form addFormSection:section];

        // - Name Input
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"name" rowType:XLFormRowDescriptorTypeName];
        [row.cellConfigAtConfigure setObject:@"Name" forKey:@"textField.placeholder"];
        [section addFormRow:row];

    // Password Section
    section = [XLFormSectionDescriptor formSectionWithTitle:@"Password"];
    [form addFormSection:section];

        // - Password Input
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"password" rowType:XLFormRowDescriptorTypePassword];
        [row.cellConfigAtConfigure setObject:@"Password" forKey:@"textField.placeholder"];
        [section addFormRow:row];

        // - Password Confirmation Input
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"confirmation" rowType:XLFormRowDescriptorTypePassword];
        [row.cellConfigAtConfigure setObject:@"Password Confirmation" forKey:@"textField.placeholder"];
        [section addFormRow:row];

    // Buttons Section
    section = [XLFormSectionDescriptor formSection];
    [form addFormSection:section];

        // - Submit Button
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"" rowType:XLFormRowDescriptorTypeButton title:@"Submit"];
        row.action.formSelector = @selector(didTouchSubmit:);
        [section addFormRow:row];

        // - Cancel Button
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"" rowType:XLFormRowDescriptorTypeButton title:@"Cancel"];
        row.action.formSelector = @selector(didTouchCancel:);
        [section addFormRow:row];

    self.form = form;
    self.tableView.rowHeight = 44;
}

- (void)initContraints
{
    // Disable the conversion from autoresizing masks into contraints
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;

    // Configure our own contraints

    // form.top == navbar.bottom
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.navBar
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1
                                                           constant:1]];
    // form.bottom == view.bottom
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1
                                                           constant:0]];
    // form.left == view.left
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    // form.right == view.right
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
}

#pragma mark - FORM CONTROL

- (void)clearRows:(NSArray<XLFormRowDescriptor*>*)rows
{
    // Clear all rows
    for (XLFormRowDescriptor* row in rows)
    {
        row.value = @"";
        [self reloadFormRow:row];
    }
}

- (void)showErrorAlert:(NSString*)message
{
    static NSString *title = @"Error";
    [UIUtil showAlertWithTitle:title andMessage:message intoViewController:self withCompletion:nil];
}

- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
    self.callback();
}

#pragma mark - EVENTS

- (IBAction)didTouchSubmit:(id)sender
{
    XLFormRowDescriptor *name         = [self.form formRowWithTag:@"name"];
    XLFormRowDescriptor *password     = [self.form formRowWithTag:@"password"];
    XLFormRowDescriptor *confirmation = [self.form formRowWithTag:@"confirmation"];

    // Name must be at least 'kNameMinLength' characters long
    if (name.value == nil || ((NSString*)name.value).length < kNameMinLength)
    {
        static NSString *message = @"Please type in your name.";

        [self showErrorAlert:message];
    }

    // Password must be at least 'kPasswordMinLength' characters long
    else if (password.value == nil || ((NSString*)password.value).length < kPasswordMinLength)
    {
        static NSString *message = @"Your password must be at least 4 characters long.";

        [self clearRows:@[password, confirmation]];
        [self showErrorAlert:message];
    }

    // Password and confirmation must match
    else if (password.value != confirmation.value)
    {
        static NSString *message = @"The passwords don't match.";

        [self clearRows:@[password, confirmation]];
        [self showErrorAlert:message];
    }

    // Everything is valid, proceed
    else
    {
        [UIUtil showHUDinView:self.view withLabel:@"Sign you up..."

            andExecute:^(void (^success)(NSString *), void (^failure)(NSString *), void (^end)(void)) {

                [User signUpWithName:name.value andPassword:password.value withCompletion:^{
                    success(@"Done!");
                    [self dismiss];
                } failure:failure];
            }
        ];
    }
}

- (IBAction)didTouchCancel:(id)sender
{
    [self dismiss];
}

@end
