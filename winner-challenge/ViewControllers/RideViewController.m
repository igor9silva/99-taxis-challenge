//
//  RideViewController.m
//  winner-challenge
//
//  Created by Igor Silva on 5/14/16.
//  Copyright © 2016 99Taxis. All rights reserved.
//

#import "RideViewController.h"
#import "UIUtil.h"

#define kResyncInterval 5 // seconds

@implementation RideViewController {
    NSTimer *_rideSyncTimer;
}

#pragma mark - LIFECYCLE

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Add an observer for ride's msg property
    [self.ride addObserver:self forKeyPath:@"msg" options:NSKeyValueObservingOptionNew context:nil];

    /**
     * Schedule a timer to check for ride updates each 'kResyncInterval' seconds.
     *
     * This would ideally be implemented using Push Notifications or any
     * other on-demand solution, to avoid performing many requests sequentially.
     */
    _rideSyncTimer = [NSTimer scheduledTimerWithTimeInterval:kResyncInterval
                                                      target:self
                                                    selector:@selector(resyncRide:)
                                                    userInfo:nil
                                                     repeats:YES];
}

-(void)dealloc
{
    [self.ride removeObserver:self forKeyPath:@"msg"];
    [_rideSyncTimer invalidate];
}

#pragma mark - EVENTS

- (IBAction)dismiss:(id)sender
{
    NSString *title = @"Cancel ride?";
    NSString *message = @"It seems the driver is on his way to pick you up. Are you sure you want to cancel?";

    [UIUtil showPromptWithTitle:title andMessage:message intoViewController:self

        yesHandler:^(UIAlertAction* action)
        {
            [self dismissViewControllerAnimated:YES completion:nil];
        }

        noHandler:nil
    ];
}

- (IBAction)arrived:(id)sender
{
    NSString *title = @"Did the taxi arrive?";
    NSString *message = @"Did your driver arrived and did you board the taxi?";

    [UIUtil showPromptWithTitle:title andMessage:message intoViewController:self

        yesHandler:^(UIAlertAction* action)
        {
            [self dismissViewControllerAnimated:YES completion:nil];
        }

        noHandler:nil
    ];
}

-(void)resyncRide:(NSTimer*)timer
{
    [self.ride sync];
}

- (void)observeValueForKeyPath:(NSString*)keyPath
                      ofObject:(id)object
                        change:(NSDictionary*)change
                       context:(void*)context {

    if ([keyPath isEqualToString:@"msg"])
    {
        // Update the label within the new value
        self.lblMsg.text = change[@"new"];
    }
}

@end
